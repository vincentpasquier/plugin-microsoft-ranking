/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.microsoft.extractors;

import ch.hesso.predict.client.APIClient;
import ch.hesso.predict.client.resources.ConferenceClient;
import ch.hesso.predict.client.resources.WebPageClient;
import ch.hesso.predict.plugins.microsoft.MicrosoftRankingUtils;
import ch.hesso.predict.restful.Ranking;
import ch.hesso.predict.restful.WebPage;
import ch.hesso.websocket.entities.AnnotatedPlugin;
import ch.hesso.websocket.plugins.PluginInterface;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class MicrosoftRankingProcessor {

	private PluginInterface.PluginStatus.Builder builder;

	private Map<Integer, String> extractedCategories;

	private Multimap<String, String> linksByCategories
			= HashMultimap.create ();

	private Set<Ranking> rankings = new HashSet<> ();

	public MicrosoftRankingProcessor () {
	}

	public void run () {
		builder = PluginInterface.newBuilderPluginStatus ( AnnotatedPlugin.State.RUNNING );
		builder.progress ( 1 );
		builder.totalProgress ( 2 );

		boolean success = extractAllPages ();
		if ( !success ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to extract all pages properly, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
			return;
		}
		builder.progress ( 2 );
		builder.totalProgress ( 2 );

		success = publishRankings ();
		if ( !success ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to publish all rankings properly, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
		} else {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.DONE )
					.text ( "Successfully extracted and synchronized conferences rankings." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
		}
	}

	private boolean extractAllPages () {
		boolean success = true;
		builder.text ( "Extracting Microsoft ranking from website. Collecting all ranks, publication count, acronyms, conference title and categories." );
		PluginInterface.publishPluginStatus ( builder.build () );

		extractedCategories = MicrosoftRankingUtils.extractCategoriesIds ();
		WebPageClient client = WebPageClient.create ( APIClient.REST_API );
		for ( Map.Entry<Integer, String> entry : extractedCategories.entrySet () ) {
			String link = MicrosoftRankingUtils.buildRankLink ( entry.getKey (), 0 );
			WebPage webPage = new WebPage ();
			webPage.setUrl ( link );
			webPage = client.getWebPage ( webPage );
			if ( client.isValidWebPage ( webPage ) ) {
				List<String> links =
						MicrosoftRankingUtils.buildLinks ( webPage.getContent (), entry.getKey () );
				linksByCategories.putAll ( entry.getValue (), links );
			}
		}

		for ( String category : linksByCategories.keySet () ) {
			for ( String link : linksByCategories.get ( category ) ) {
				WebPage webPage = new WebPage ();
				webPage.setUrl ( link );
				webPage = client.getWebPage ( webPage );
				rankings.addAll (
						MicrosoftRankingUtils.extractFromWebPage ( category, webPage.getContent () ) );
			}
		}

		return success;
	}

	private boolean publishRankings () {
		boolean success = true;
		builder.text ( "Publishing ranking to triple store. Each ranking updates conferences." );
		PluginInterface.publishPluginStatus ( builder.build () );

		ConferenceClient conferenceClient = ConferenceClient.create ( APIClient.REST_API );
		for ( Ranking ranking : rankings ) {
			conferenceClient.addRanking ( ranking );
		}

		return success;
	}
}
