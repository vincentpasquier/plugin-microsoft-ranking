/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.microsoft;

import ch.hesso.commons.HTTPMethod;
import ch.hesso.predict.plugins.microsoft.extractors.MicrosoftRankingProcessor;
import ch.hesso.predict.restful.APIEntity;
import ch.hesso.predict.restful.visitors.APIEntityVisitor;
import ch.hesso.websocket.annotations.CallableMethod;
import ch.hesso.websocket.plugins.PluginInterface;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class MicrosoftRankingPluginInterface extends PluginInterface {

	private final Set<APIEntityVisitor> _visitors = new HashSet<> ();

	@CallableMethod ( value = "Extract rankings",
			description = "Extract ranking (publication count, field rating) and conferences categories. Perform the whole extraction in a single pass. Synchronizes with the triplestore." )
	public void extractRankings () {
		MicrosoftRankingProcessor processor = new MicrosoftRankingProcessor ();
		processor.run ();
	}

	@Override
	public String name () {
		return "Microsoft Academic Research Ranking Extractor";
	}

	@Override
	public String description () {
		return "Extractor aimed at the <a href='http://academic.research.microsoft.com/' target='_blank'>Microsoft Academic Research website</a>. It extracts each conference and its respective field rating (modified h-index) as well as publication count. Category annotation for each conference also provide help when browsing large corpus. This extractor goal is to provide other tools better results in recommendation.";
	}

	@Override
	public Map<Class<? extends APIEntity>, Set<HTTPMethod>> resources () {
		Map<Class<? extends APIEntity>, Set<HTTPMethod>> resources = new HashMap<> ();
		//resources.put ( Conference.class, ImmutableSet.of ( HTTPMethod.POST ) );
		return resources;
	}

	@Override
	public Set<APIEntityVisitor> visitors () {
		return _visitors;
	}
}
