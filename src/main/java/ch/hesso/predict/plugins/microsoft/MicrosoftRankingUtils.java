/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.microsoft;

import ch.hesso.predict.client.APIClient;
import ch.hesso.predict.client.resources.WebPageClient;
import ch.hesso.predict.restful.Ranking;
import ch.hesso.predict.restful.WebPage;
import com.google.common.base.Joiner;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public final class MicrosoftRankingUtils {

	private static final String ROOT =
			"http://academic.research.microsoft.com";

	private static final String RANK_PAGE = ROOT + "/RankList?";

	private static final String CONF_ENTITY = "entitytype=3";

	private static final String COMPUTER_DOMAIN = "topDomainID=2";

	private static final String SUBDOMAINS = "subDomainID=";

	private static final String LAST_STR = "last=0";

	private static final String START_STR = "start=";

	private static final String END_STR = "end=";

	private static final int PAGE_INCREMENT = 100;

	private static final int PAGE_START = 1;

	private static final String START_PAGE =
			ROOT + "/?SearchDomain=2&SubDomain=0&entitytype=3";

	private static final Pattern EXTRACT_SUBDOMAIN =
			Pattern.compile ( "SubDomain=([0-9]{1,2})" );

	private static final Pattern EXTRACT_RESULTS_NUMBER =
			Pattern.compile ( "([0-9]{1,5}) results" );

	private static final Pattern EXTRACT_ACRONYM =
			Pattern.compile ( "^([A-z0-9\\-\\*]{1,10})( \\-|\\(|/)" );

	private MicrosoftRankingUtils () {
		throw new AssertionError ( "Utility class, do not instanciate ! :D" );
	}

	public static Map<Integer, String> extractCategoriesIds () {
		Map<Integer, String> categoriesLinks = new HashMap<> ();
		WebPageClient client = WebPageClient.create ( APIClient.REST_API );
		WebPage webPage = new WebPage ();
		webPage.setUrl ( START_PAGE );
		webPage = client.getWebPage ( webPage );
		if ( client.isValidWebPage ( webPage ) ) {
			Document doc = Jsoup.parse ( webPage.getContent () );
			Elements elements = doc.select ( "li.subdomain-list-item-indent" );
			for ( Element element : elements.select ( "a" ) ) {
				String link = element.attr ( "href" );
				Matcher matcher = EXTRACT_SUBDOMAIN.matcher ( link );
				if ( matcher.find () ) {
					int id = Integer.parseInt ( matcher.group ( 1 ) );
					String category = Parser.unescapeEntities ( element.text (), true );
					categoriesLinks.put ( id, category );
				}
			}
		}
		return categoriesLinks;
	}

	public static Set<Ranking> extractFromWebPage ( final String category, final String content ) {
		Set<Ranking> rankings = new HashSet<> ();
		Document doc = Jsoup.parse ( content );
		Elements rows = doc.select ( ".staticTable > tbody > tr" );
		for ( Element row : rows ) {
			Element eTitle = row.select ( ".rank-content" ).first ();
			Elements ePubFields = row.select ( ".staticOrderCol" );
			if ( ePubFields.size () < 2 ) {
				break;
			}
			Element ePublications = ePubFields.first ();
			Element eFieldRating = ePubFields.get ( 1 );
			if ( eTitle != null && ePublications != null && eFieldRating != null ) {
				Ranking rank = new Ranking ();
				String title = eTitle.text ();
				rank.setTitle ( title );
				String acronym = "";
				Matcher matcher = EXTRACT_ACRONYM.matcher ( title );
				if ( matcher.find () ) {
					acronym = matcher.group ( 1 ).toLowerCase ();
				}
				rank.setAcronym ( acronym );
				int countPublications = Integer.parseInt ( ePublications.text () );
				rank.setPublication ( countPublications );
				int fieldRating = Integer.parseInt ( eFieldRating.text () );
				rank.setFieldRating ( fieldRating );
				rank.setCategory ( category );
				rankings.add ( rank );
			}
		}
		return rankings;
	}

	public static List<String> buildLinks ( final String content, final int subDomainId ) {
		List<String> links = new ArrayList<> ();
		Document doc = Jsoup.parse ( content );
		String nbResults = doc.select ( ".ranklistresult" ).first ().text ();
		Matcher matcher = EXTRACT_RESULTS_NUMBER.matcher ( nbResults );
		int results = 0;
		if ( matcher.find () ) {
			results = Integer.parseInt ( matcher.group ( 1 ) );
		}
		int linksToGenerate = ( results / PAGE_INCREMENT ) + 1;
		for ( int i = 0; i < linksToGenerate; i++ ) {
			links.add ( buildRankLink ( subDomainId, i ) );
		}
		return links;
	}

	public static String buildRankLink ( final int subDomainId, final int page ) {
		String start = START_STR + ( ( page * PAGE_INCREMENT ) + 1 );
		String end = END_STR + ( ( page * PAGE_INCREMENT ) + PAGE_INCREMENT );
		String subDomain = SUBDOMAINS + subDomainId;
		String link =
				Joiner
						.on ( "&" )
						.join ( CONF_ENTITY, COMPUTER_DOMAIN, subDomain, LAST_STR, start, end );

		return RANK_PAGE + link;
	}

}
